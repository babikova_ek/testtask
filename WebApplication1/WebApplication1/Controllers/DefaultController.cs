﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    public class NotificationsController : Controller
    {
        [Produces("text/plain")]
        [HttpGet("notifications")]
        public async Task<IActionResult> SendMessage(
            [FromQuery(Name = "token")] string token,
            [FromQuery(Name = "user-key")] string userKey,
            [FromQuery(Name = "message")] string message)
        {
            try
            {
                Send(token, userKey, message);

                return Ok("Your message has been send");
            }
            catch (Exception e)
            {
                return BadRequest("ERROR: " + e.Message);
            }
        }

        private void Send(string token, string userKey, string message)
        {
            var parameters = new NameValueCollection {
                    { "token", token},
                    { "user", userKey },
                    { "message", message }
                };

            using (var client = new WebClient())
            {
                client.UploadValues("https://api.pushover.net/1/messages.json", parameters);
            }
        }
    }

}